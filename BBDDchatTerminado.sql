/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.3.16-MariaDB : Database - bdproyect
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bdproyect` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `bdproyect`;

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `Id_Us` int(11) NOT NULL AUTO_INCREMENT,
  `Correo` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `Apellidos` varchar(100) DEFAULT NULL,
  `Puesto` varchar(100) DEFAULT NULL,
  `Conocimientos` longtext DEFAULT NULL,
  `Aficiones` longtext DEFAULT NULL,
  `Foto` varchar(100) DEFAULT NULL,
  `Fecha_Nac` date DEFAULT NULL,
  `Fecha_Ult_Con` datetime DEFAULT NULL,
  `foto_archivo` blob DEFAULT NULL,
  `fechamodificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`Id_Us`),
  UNIQUE KEY `idx_usu_correo` (`Correo`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;


/*Table structure for table `canales` */

DROP TABLE IF EXISTS `canales`;

CREATE TABLE `canales` (
  `Id_Canal` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `Descripcion` longtext DEFAULT NULL,
  `Imagen` blob DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`Id_Canal`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `canales` */

/*Table structure for table `conversa` */

DROP TABLE IF EXISTS `conversa`;

CREATE TABLE `conversa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Id_Us` int(11) DEFAULT NULL,
  `Id_Canal` int(11) DEFAULT NULL,
  `Mensaje` longtext DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id_Us` (`Id_Us`),
  KEY `Id_Canal` (`Id_Canal`),
  CONSTRAINT `conversa_ibfk_1` FOREIGN KEY (`Id_Us`) REFERENCES `usuarios` (`Id_Us`),
  CONSTRAINT `conversa_ibfk_2` FOREIGN KEY (`Id_Canal`) REFERENCES `canales` (`Id_Canal`)
) ENGINE=InnoDB AUTO_INCREMENT=237 DEFAULT CHARSET=utf8;

/*Data for the table `conversa` */

/*Table structure for table `migration_versions` */

DROP TABLE IF EXISTS `migration_versions`;

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migration_versions` */

/*Table structure for table `u_c` */

DROP TABLE IF EXISTS `u_c`;

CREATE TABLE `u_c` (
  `Id_Us` int(11) DEFAULT NULL,
  `Id_Canal` int(11) DEFAULT NULL,
  `Id_UC` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha_Inscripcion` datetime DEFAULT NULL,
  PRIMARY KEY (`Id_UC`),
  UNIQUE KEY `unique` (`Id_Us`,`Id_Canal`),
  KEY `us` (`Id_Us`),
  KEY `ca` (`Id_Canal`),
  CONSTRAINT `ca-pk` FOREIGN KEY (`Id_Canal`) REFERENCES `canales` (`Id_Canal`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `us-pk` FOREIGN KEY (`Id_Us`) REFERENCES `usuarios` (`Id_Us`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

/*Data for the table `u_c` */


/*Data for the table `usuarios` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
