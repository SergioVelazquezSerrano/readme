## BBDD

En nuestro phpmyadmin tendremos que importar el archivo **BBDDchatTerminado** que se encuentra en este repositorio.

La BBDD esta vinculada en ambos proyectos en el archivo .env 

**# CONEXION EN CASA**
**DATABASE_URL=mysql://root:@localhost:3306/bdproyect**

## Admin Chat

En el repositorio Admin Chat de este proyecto se encuentra la App de gestión de usuarios y canales que nos pidio el cliente.

Arrancando dicho proyecto con **symfony server:start** nos arrancara el servidor dandonos su ruta web, simplemente tendremos que añadirle **/admin** para entrar a la web que deseamos.

## Proyect Chat

En el repositorio Proyect Chat de este proyecto se encuentra la App de comunicación asincrona que nos pidio el cliente

Arrancando dicho proyecto con **symfony server:start** nos arrancara el servidor dandonos su ruta web, simplemente tendremos que añadirle **/register** para darse de alta en la aplicación, y **/login** para poder empezar a utilizar la aplicación.

## Readme

En el repositorio Readme encontraremos información, memoria del proyecto y powerpoint de dicho.